import { createRouter, createWebHistory } from 'vue-router'
import layout from '@/layout/layout.vue'

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/',
            component: layout,
            name: 'layout',
            children: [
                {
                    path: '/',
                    name: 'home',
                    component: () => import('@/views/index.vue'),
                    meta: {
                        title: '首页'
                    }
                }
            ]
        },
        {
            path: '/login',
            name: 'login',
            component: () => import('@/views/login.vue'),
            meta: {
                title: '登录'
            }
        }
    ]
})

export default router
