import { defineStore } from 'pinia'

export const useAppStore = defineStore('appStore', {
    state: () => {
        return {
            asideWidth: '200px'
        }
    },
    getters: {
        getAsideWidth(): string {
            return this.asideWidth
        },
        getAsideExpand(): boolean {
            return '200px' === this.asideWidth
        }
    },
    actions: {
        toggleAside() {
            this.asideWidth = '200px' === this.asideWidth ? '64px' : '200px'
        }
    }
})
