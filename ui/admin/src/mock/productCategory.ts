import type { MockMethod } from 'vite-plugin-mock'

export default [
    {
        url: '/api/product/category/list',
        method: 'get',
        response: () => {
            return {
                code: '200',
                msg: '操作成功',
                success: true,
                body: [
                    {
                        id: '1',
                        parentId: '0',
                        name: '家居厨具',
                        icon: null,
                        sortNum: 1,
                        visible: '0',
                        recommend: 'N',
                        createTime: '2023-12-27 13:39:40',
                        children: [
                            {
                                id: '2',
                                parentId: '1',
                                name: '日常用具',
                                icon: null,
                                sortNum: 1,
                                visible: '0',
                                recommend: 'N',
                                createTime: '2023-12-27 13:39:40',
                                children: [
                                    {
                                        id: '3',
                                        parentId: '2',
                                        name: '空气炸锅',
                                        icon: null,
                                        sortNum: 1,
                                        visible: '0',
                                        recommend: 'N',
                                        createTime: '2023-12-27 13:39:40',
                                        children: null
                                    },
                                    {
                                        id: '4',
                                        parentId: '2',
                                        name: '锅具',
                                        icon: null,
                                        sortNum: 2,
                                        visible: '0',
                                        recommend: 'N',
                                        createTime: '2023-12-27 13:39:40',
                                        children: null
                                    },
                                    {
                                        id: '5',
                                        parentId: '2',
                                        name: '破壁机',
                                        icon: null,
                                        sortNum: 3,
                                        visible: '0',
                                        recommend: 'N',
                                        createTime: '2023-12-27 13:39:40',
                                        children: null
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        id: '6',
                        parentId: '0',
                        name: '户外运动',
                        icon: null,
                        sortNum: 2,
                        visible: '0',
                        recommend: 'N',
                        createTime: '2023-12-27 13:39:40',
                        children: [
                            {
                                id: '7',
                                parentId: '6',
                                name: '户外用品',
                                icon: null,
                                sortNum: 1,
                                visible: '0',
                                recommend: 'N',
                                createTime: '2023-12-27 13:39:40',
                                children: [
                                    {
                                        id: '8',
                                        parentId: '7',
                                        name: '降落伞',
                                        icon: null,
                                        sortNum: 1,
                                        visible: '0',
                                        recommend: 'N',
                                        createTime: '2023-12-27 13:39:40',
                                        children: null
                                    },
                                    {
                                        id: '9',
                                        parentId: '7',
                                        name: '水杯',
                                        icon: null,
                                        sortNum: 2,
                                        visible: '0',
                                        recommend: 'N',
                                        createTime: '2023-12-27 13:39:40',
                                        children: null
                                    },
                                    {
                                        id: '10',
                                        parentId: '7',
                                        name: '露营',
                                        icon: null,
                                        sortNum: 3,
                                        visible: '0',
                                        recommend: 'N',
                                        createTime: '2023-12-27 13:39:40',
                                        children: null
                                    }
                                ]
                            }
                        ]
                    }
                ],
                extra: 'mock'
            }
        }
    },
    {
        url: '/api/product/category/editEcho/:id',
        method: 'get',
        response: () => {
            return {
                code: '200',
                msg: '操作成功',
                success: true,
                body: {},
                extra: 'mock'
            }
        }
    },
    {
        url: '/api/product/category/edit',
        method: 'put',
        response: () => {
            return {
                code: '200',
                msg: '操作成功',
                success: true,
                body: {},
                extra: 'mock'
            }
        }
    },
    {
        url: '/api/product/category/delete/:id',
        method: 'delete',
        response: (ctx: any) => {
            console.log(typeof ctx.query.id)
            if ('1' === ctx.query.id) {
                return {
                    code: 'product_category_has_child',
                    msg: '当前分类还有子分类',
                    success: false,
                    body: {},
                    extra: 'mock'
                }
            }
            return {
                code: '200',
                msg: '操作成功',
                success: true,
                body: {},
                extra: 'mock'
            }
        }
    },
    {
        url: '/api/product/category/create',
        method: 'post',
        response: () => {
            return {
                code: '200',
                msg: '操作成功',
                success: true,
                body: {},
                extra: 'mock'
            }
        }
    }
] as MockMethod[]
