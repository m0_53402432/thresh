import type { MockMethod } from 'vite-plugin-mock'

export default [
    {
        url: '/api/index/test',
        method: 'get',
        response: () => {
            return {
                code: '200',
                msg: '操作成功',
                success: true,
                body: {
                    token: 'index test'
                },
                extra: 'mock'
            }
        }
    }
] as MockMethod[]
