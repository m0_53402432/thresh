import request from '@/utils/request'
import type { AxiosResponse } from 'axios'
import type { R } from '@/utils/common'

export default {
    test(): Promise<AxiosResponse<R<any>>> {
        return request.request({
            url: '/api/index/test',
            method: 'GET'
        }) as Promise<AxiosResponse<R<any>>>
    }
}
