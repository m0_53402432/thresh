export interface ProductCategoryList {
    id: string
    parentId: string
    name: string
    icon: string
    sortNum: number
    visible: string
    recommend: string
    createTime: string
    children: Array<ProductCategoryList> | null
}
