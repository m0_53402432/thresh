import request from '@/utils/request'
import type { AxiosResponse } from 'axios'
import type { R } from '@/utils/common'

export default {
    sayHello(): Promise<AxiosResponse<R<any>>> {
        return request.request({
            url: '/api/check/pipeline',
            method: 'GET'
        }) as Promise<AxiosResponse<R<any>>>
    }
}
