import request from '@/utils/request'
import type { AxiosResponse } from 'axios'
import type { R } from '@/utils/common'

export default {
    list(): Promise<AxiosResponse<R<any>>> {
        return request.request({
            url: '/api/product/category/list',
            method: 'GET'
        }) as Promise<AxiosResponse<R<any>>>
    },
    editEcho(id: string): Promise<AxiosResponse<R<any>>> {
        return request.request({
            url: `/api/product/category/editEcho/${id}`,
            method: 'GET'
        }) as Promise<AxiosResponse<R<any>>>
    },
    edit(data: object): Promise<AxiosResponse<R<any>>> {
        return request.request({
            url: '/api/product/category/edit',
            method: 'PUT',
            data
        }) as Promise<AxiosResponse<R<any>>>
    },
    delete(id: string): Promise<AxiosResponse<R<any>>> {
        return request.request({
            url: `/api/product/category/delete/${id}`,
            method: 'DELETE'
        }) as Promise<AxiosResponse<R<any>>>
    },
    create(data: object): Promise<AxiosResponse<R<any>>> {
        return request.request({
            url: '/api/product/category/create',
            method: 'POST',
            data
        }) as Promise<AxiosResponse<R<any>>>
    }
}
