import request from '@/utils/request'
import type { AxiosResponse } from 'axios'
import type { R } from '@/utils/common'

export default {
    login(data: object): Promise<AxiosResponse<R<any>>> {
        return request.request({
            url: '/api/auth/login',
            method: 'POST',
            data
        }) as Promise<AxiosResponse<R<any>>>
    },
    logout(): Promise<AxiosResponse<R<any>>> {
        return request.request({
            url: '/api/auth/logout',
            method: 'GET'
        }) as Promise<AxiosResponse<R<any>>>
    },
    userInfo(): Promise<AxiosResponse<R<any>>> {
        return request.request({
            url: '/api/auth/userInfo',
            method: 'get'
        }) as Promise<AxiosResponse<R<any>>>
    }
}
