import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/',
            name: 'home',
            component: () => import('@/views/index.vue'),
            meta: {
                title: '首页'
            }
        },
        {
            path: '/:pathMatch(.*).*',
            name: 'NotFound',
            component: () => import('@/views/404.vue')
        }
    ]
})

export default router
