import './assets/reset.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'

import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'

import App from './App.vue'
import router from './router'

const app = createApp(App)

app.use(ElementPlus)

// element plus icon
import * as ElementPlusIconsVue from '@element-plus/icons-vue'

for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}

import 'virtual:windi.css'

app.use(createPinia())
app.use(router)

// nprogress
import 'nprogress/nprogress.css'

// iconfont
import '@/assets/iconfont/iconfont.css'

app.mount('#app')
