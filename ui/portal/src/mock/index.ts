import type { MockMethod } from 'vite-plugin-mock'

export default [
    {
        url: '/api/check/pipeline',
        method: 'get',
        response: () => {
            return {
                code: '200',
                msg: '操作成功',
                success: true,
                body: null,
                extra: 'mock'
            }
        }
    }
] as MockMethod[]
