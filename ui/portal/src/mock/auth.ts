import type { MockMethod } from 'vite-plugin-mock'

export default [
    {
        url: '/api/auth/login',
        method: 'post',
        response: () => {
            return {
                code: '200',
                msg: '操作成功',
                success: true,
                body: {
                    token: 'token_mock'
                },
                extra: 'mock'
            }
        }
    },
    {
        url: '/api/auth/logout',
        method: 'get',
        response: () => {
            return {
                code: '200',
                msg: '操作成功',
                success: true,
                body: null,
                extra: 'mock'
            }
        }
    },
    {
        url: '/api/auth/userInfo',
        method: 'get',
        response: () => {
            return {
                code: '200',
                msg: '操作成功',
                success: true,
                body: {
                    user: {
                        phone: '13500001001',
                        username: 'superAdmin',
                        nickname: '超级管理员',
                        avatar: 'http://image.jx.com/user/1/2023/12/24/avatar.jpg',
                        email: 'superAdmin@jx.com'
                    },
                    perms: ['*:*:*'],
                    roles: ['superAdmin'],
                    routers: [
                        {
                            name: 'Product',
                            path: 'product',
                            component: 'layout',
                            alwayShow: true,
                            meta: {
                                title: '商品',
                                icon: 'icon iconfont icon-31quanbushangpin'
                            },
                            children: [
                                {
                                    name: 'ProductList',
                                    path: 'list',
                                    component: 'product/product',
                                    alwayShow: true,
                                    meta: {
                                        title: '商品管理',
                                        icon: 'Aim'
                                    },
                                    children: []
                                },
                                {
                                    name: 'ProductCategory',
                                    path: 'category',
                                    component: 'product/category',
                                    alwayShow: true,
                                    meta: {
                                        title: '商品分类',
                                        icon: 'Aim'
                                    },
                                    children: []
                                },
                                {
                                    name: 'ProductBrand',
                                    path: 'brand',
                                    component: 'layout',
                                    alwayShow: true,
                                    meta: {
                                        title: '品牌管理',
                                        icon: 'Aim'
                                    },
                                    children: [
                                        {
                                            name: 'ProductBrandCategory',
                                            path: 'category',
                                            component: 'product/brand/category',
                                            alwayShow: true,
                                            meta: {
                                                title: '品牌分类',
                                                icon: 'Aim'
                                            },
                                            children: []
                                        },
                                        {
                                            name: 'ProductBrandList',
                                            path: 'list',
                                            component: 'product/brand/brand',
                                            alwayShow: true,
                                            meta: {
                                                title: '品牌列表',
                                                icon: 'Aim'
                                            },
                                            children: []
                                        }
                                    ]
                                },
                                {
                                    name: 'ProductComment',
                                    path: 'comment',
                                    component: 'product/comment',
                                    alwayShow: true,
                                    meta: {
                                        title: '评论管理',
                                        icon: 'Aim'
                                    },
                                    children: []
                                },
                                {
                                    name: 'ProductGuanrantee',
                                    path: 'guanrantee',
                                    component: 'product/guanrantee',
                                    alwayShow: true,
                                    meta: {
                                        title: '保障服务',
                                        icon: 'Aim'
                                    },
                                    children: []
                                },
                                {
                                    name: 'ProductLabel',
                                    path: 'label',
                                    component: 'product/label',
                                    alwayShow: true,
                                    meta: {
                                        title: '商品标签',
                                        icon: 'Aim'
                                    },
                                    children: []
                                },
                                {
                                    name: 'ProductSpec',
                                    path: 'spec',
                                    component: 'layout',
                                    alwayShow: true,
                                    meta: {
                                        title: '商品参数',
                                        icon: 'Aim'
                                    },
                                    children: [
                                        {
                                            name: 'ProductSpecMerchantSpec',
                                            path: 'merchantSpec',
                                            component: 'product/spec/merchantSpec',
                                            alwayShow: true,
                                            meta: {
                                                title: '商户商品参数',
                                                icon: 'Aim'
                                            },
                                            children: []
                                        },
                                        {
                                            name: 'ProductSpecPlatformSpec',
                                            path: 'platformSpec',
                                            component: 'product/spec/platformSpec',
                                            alwayShow: true,
                                            meta: {
                                                title: '平台商品参数',
                                                icon: 'Aim'
                                            },
                                            children: []
                                        }
                                    ]
                                },
                                {
                                    name: 'ProductPriceDesc',
                                    path: 'priceDesc',
                                    component: 'product/priceDesc',
                                    alwayShow: true,
                                    meta: {
                                        title: '价格说明',
                                        icon: 'Aim'
                                    },
                                    children: []
                                }
                            ]
                        },
                        {
                            name: 'Setting',
                            path: 'setting',
                            component: 'layout',
                            alwayShow: true,
                            meta: {
                                title: '设置',
                                icon: 'Setting'
                            },
                            children: [
                                {
                                    name: 'SettingNotification',
                                    path: 'notification',
                                    component: 'layout',
                                    alwayShow: true,
                                    meta: {
                                        title: '消息管理',
                                        icon: 'Aim'
                                    },
                                    children: [
                                        {
                                            name: 'SettingNotificationNotice',
                                            path: 'notice',
                                            component: 'setting/notification/notice',
                                            alwayShow: true,
                                            meta: {
                                                title: '公告管理',
                                                icon: 'Aim'
                                            },
                                            children: []
                                        },
                                        {
                                            name: 'SettingNotificationMessage',
                                            path: 'message',
                                            component: 'setting/notification/message',
                                            alwayShow: true,
                                            meta: {
                                                title: '消息管理',
                                                icon: 'Aim'
                                            },
                                            children: []
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                },
                extra: 'mock'
            }
        }
    }
] as MockMethod[]
