import { fileURLToPath, URL } from 'node:url'

import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import WindiCSS from 'vite-plugin-windicss'
import { viteMockServe } from 'vite-plugin-mock'

export default defineConfig(({ mode }) => {
    const env = loadEnv(mode, process.cwd())
    console.log(mode)
    console.log(env)
    const plugins = [vue(), WindiCSS()]
    if ('mock' === mode) {
        plugins.push(
            viteMockServe({
                logger: true,
                mockPath: './src/mock/'
            })
        )
    }
    return {
        plugins: plugins,
        resolve: {
            alias: {
                '@': fileURLToPath(new URL('./src', import.meta.url))
            }
        },

        server: {
            port: 5177,
            proxy: {
                '/api': {
                    target: env.VITE_API_BASE_URL,
                    changeOrigin: true,
                    rewrite: (path) => path.replace(/^\/api/, '')
                }
            }
        },

        css: {
            preprocessorOptions: {
                scss: {
                    additionalData: '@import "@/style/global.scss";'
                }
            }
        }
    }
})
