# 基础镜像使用 java8
FROM java:8
# 作者
LABEL maintainer="khl <xiaodaima2016@163.comm>"
# 修正时区问题
RUN bash -c 'echo "Asia/Shanghai" > /etc/timezone'
# 端口
EXPOSE 8080
# 挂载目录
VOLUME /usr/local/thresh/conf
VOLUME /usr/local/thresh/log
VOLUME /usr/local/thresh/agent
# 将jar包添加到容器中并更名为app.jar
ADD thresh-app/target/thresh-app.jar /usr/local/thresh/app/app.jar
# skywalking

ENV THRESH_SW_AGENT_NAME=thresh

# 运行jar包
RUN bash -c 'touch /app.jar'
ENTRYPOINT [ "java", \
    "-Djava.security.egd=file:/dev/./urandom", \
    "-Dspring.config.location=/usr/local/thresh/conf/", \
    "-Dspring.profiles.active=test", \
    "-javaagent:/usr/local/thresh/agent/skywalking-agent/skywalking-agent.jar", \
    "-DSW_AGENT_NAME=${THRESH_SW_AGENT_NAME}", \
    "-DSW_AGENT_COLLECTOR_BACKEND_SERVICES=172.24.0.2:11800", \
    "-jar", \
    "/usr/local/thresh/app/app.jar" \
]