pipeline {
    agent any

    parameters {
        gitParameter branch: '', branchFilter: '.*', defaultValue: 'origin/main', description: '分支', name: 'branch', quickFilterEnabled: false, selectedValue: 'NONE', sortMode: 'NONE', tagFilter: '*', type: 'GitParameterDefinition'
    }

    stages {
        // 拉取代码
        stage('pull code') {
            steps {
                checkout scmGit(branches: [[name: "${params.branch}"]], extensions: [], userRemoteConfigs: [[url: 'https://gitcode.com/m0_53402432/thresh.git']])
            }
        }

        // 构建前端项目
        stage('build ui') {
            steps {
                parallel(
                    'build portal': {
                        dir('ui/portal') {
                            sh 'npm install'
                            sh 'npm run build-test'
                            sh 'docker build -t khl/thresh-test-ui-portal:0.1 .'
                        }                        
                    },
                    'build admin': {
                        dir('ui/admin') {
                            sh 'npm install'
                            sh 'npm run build-test'
                            sh 'docker build -t khl/thresh-test-ui-admin:0.1 .'
                        }                        
                    },
                    'build seller': {
                        dir('ui/seller') {
                            sh 'npm install'
                            sh 'npm run build-test'
                            sh 'docker build -t khl/thresh-test-ui-seller:0.1 .'
                        }                        
                    },
                )
            }
        }

        // 构建后端项目
        stage('build spring boot') {
            steps {
                sh '''mvn -B clean package -Ptest -Dmaven.test.skip=true'''
                sh '''docker build -t khl/thresh-test-app:0.1 .'''
            }
        }

        // 停止服务
        stage('stop server') {
            steps {
                sh '''docker-compose down'''
            }
        }

        // 启动服务
        stage('start server') {
            steps {
                sh 'docker-compose --env-file=docker.env up -d'
            }
        }

        // 导入数据
        stage('import data') {
            steps{
                sleep time: 15, unit: 'SECONDS'
                sh '''docker exec -i thresh-test-mysql mysql -uroot -proot -e "drop database if exists thresh;"'''
                sleep time: 5, unit: 'SECONDS'
                sh '''docker exec -i thresh-test-mysql mysql -uroot -proot -e "create database thresh character set utf8mb4 collate utf8mb4_general_ci;"'''
                sleep time: 5, unit: 'SECONDS'
                sh '''docker exec -i thresh-test-mysql mysql -uroot -proot thresh < ./dox/docker_test/thresh.sql'''
            }
        }

        // 校验服务是否正常启动
        stage('check server') {
            steps {
                script {
                    // 等待 60 秒, 让服务完全启动
                    sleep time: 60, unit: 'SECONDS'
                    def response = httpRequest  contentType: 'APPLICATION_JSON_UTF8',
                                                responseHandle: 'LEAVE_OPEN',
                                                url: 'http://api.thresh-test.com/check/pipeline',
                                                validResponseCodes: '200',
                                                wrapAsMultipart: false
                    println("Response: ${response.content}")
                    def json = new groovy.json.JsonSlurper().parseText(response.content)
                    println(json.code)
                    assert '200' == json.code : '校验接口业务状态码不是 200'
                    response.close()
                }
            }
        }
    }
}
