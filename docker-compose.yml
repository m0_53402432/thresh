version: '3'
services:
  # skywalking 后端
  thresh-test-skywalking-oap:
    image: apache/skywalking-oap-server:8.9.0
    container_name: thresh-test-skywalking-oap
    environment:
        TZ: Asia/Shanghai
    ports:
      - 12201:11800
      - 12202:12800
    healthcheck:
      test: [ "CMD-SHELL", "/skywalking/bin/swctl ch" ]
      interval: 30s
      timeout: 10s
      retries: 3
      start_period: 10s
    networks:
      thresh-test:
        ipv4_address: ${THRESH_DEV_NETWORK_IP_PREFIX}.0.2

  # skywalking ui
  thresh-test-skywalking-ui:
    image: apache/skywalking-ui:8.9.0
    container_name: thresh-test-skywalking-ui
    links:
      - thresh-test-skywalking-oap
    environment:
      TZ: Asia/Shanghai
      SW_OAP_ADDRESS: http://thresh-test-skywalking-oap:12800
    depends_on:
      - thresh-test-skywalking-oap
    ports:
      - 12203:8080
    networks:
      thresh-test:
        ipv4_address: ${THRESH_DEV_NETWORK_IP_PREFIX}.0.3

  # es
  thresh-test-elasticsearch:
    image: elasticsearch:7.14.1
    container_name: thresh-test-elasticsearch
    restart: unless-stopped
    volumes:
      - "${THRESH_DEV_BASE_DIR}/es/data:/usr/share/elasticsearch/data"
      - "${THRESH_DEV_BASE_DIR}/es/logs:/usr/share/elasticsearch/logs"
      - "${THRESH_DEV_BASE_DIR}/es/config/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml"
      - "${THRESH_DEV_BASE_DIR}/es/plugins:/usr/share/elasticsearch/plugins"
    environment:
      TZ: Asia/Shanghai
      LANG: en_US.UTF-8
      discovery.type: single-node
      ES_JAVA_OPTS: "-Xmx512m -Xms512m"
    ports:
      - "12204:9200"
      - "12205:9300"
    networks:
      thresh-test:
        ipv4_address: ${THRESH_DEV_NETWORK_IP_PREFIX}.0.4

  # kibana
  thresh-test-kibana:
    image: kibana:7.14.1
    container_name: thresh-test-kibana
    restart: unless-stopped
    volumes:
      - ${THRESH_DEV_BASE_DIR}/kibana/config/kibana.yml:/usr/share/kibana/config/kibana.yml
    ports:
      - "12206:5601"
    depends_on:
      - thresh-test-elasticsearch
    links:
      - thresh-test-elasticsearch
    networks:
      thresh-test:
        ipv4_address: ${THRESH_DEV_NETWORK_IP_PREFIX}.0.5

  # logstash
  thresh-test-logstash:
    image: logstash:7.14.1
    container_name: thresh-test-logstash
    restart: unless-stopped
    environment:
      LS_JAVA_OPTS: "-Xmx512m -Xms512m"
    volumes:
      - "${THRESH_DEV_BASE_DIR}/logstash/data:/usr/share/logstash/data"
      - "${THRESH_DEV_BASE_DIR}/logstash/config/logstash.yml:/usr/share/logstash/config/logstash.yml"
      - "${THRESH_DEV_BASE_DIR}/logstash/config/small-tools:/usr/share/logstash/config/small-tools"
    command: logstash -f /usr/share/logstash/config/small-tools       # 指定logstash启动时使用的配置文件 - 指定目录夹（系统会自动读取文件夹下所有配置文件，并在内存中整合）
    ports:
      - "12207:9600"
      - "12208:1218"
      - "12209:20010"
      - "12210:20030"
      - "12211:20040"
    depends_on:
      - thresh-test-elasticsearch
    links:
      - thresh-test-elasticsearch
    networks:
      thresh-test:
        ipv4_address: ${THRESH_DEV_NETWORK_IP_PREFIX}.0.6

  # mysql
  thresh-test-mysql:
    image: mysql:5.7
    privileged: true
    container_name: thresh-test-mysql
    ports:
      - "12212:3306"
    volumes:
      - ${THRESH_DEV_BASE_DIR}/mysql/standalone/conf/my.cnf:/etc/mysql/mysql.conf.d/mysqld.cnf
      - ${THRESH_DEV_BASE_DIR}/mysql/standalone/data:/var/lib/mysql
      - ${THRESH_DEV_BASE_DIR}/mysql/standalone/log:/var/log/mysql
    networks:
      thresh-test:
        ipv4_address: ${THRESH_DEV_NETWORK_IP_PREFIX}.0.7
    environment:
      MYSQL_ROOT_PASSWORD: root

  # 第一个后端应用
  thresh-test-app-01:
    image: khl/thresh-test-app:0.1
    container_name: thresh-test-app-01
    ports:
      - "12213:8090"
    volumes:
      - ${THRESH_DEV_BASE_DIR}/app01/conf:/usr/local/thresh/conf
      - ${THRESH_DEV_BASE_DIR}/app01/log:/usr/local/thresh/log
      - ${THRESH_DEV_BASE_DIR}/app01/agent:/usr/local/thresh/agent
    environment:
      THRESH_SW_AGENT_NAME: thresh-test-app-01
    networks:
      thresh-test:
        ipv4_address: ${THRESH_DEV_NETWORK_IP_PREFIX}.0.8
    depends_on:
      - thresh-test-mysql

  # 第二个后端应用
  thresh-test-app-02:
    image: khl/thresh-test-app:0.1
    container_name: thresh-test-app-02
    ports:
      - "12214:8090"
    volumes:
      - ${THRESH_DEV_BASE_DIR}/app02/conf:/usr/local/thresh/conf
      - ${THRESH_DEV_BASE_DIR}/app02/log:/usr/local/thresh/log
      - ${THRESH_DEV_BASE_DIR}/app02/agent:/usr/local/thresh/agent
    environment:
      THRESH_SW_AGENT_NAME: thresh-test-app-02
    networks:
      thresh-test:
        ipv4_address: ${THRESH_DEV_NETWORK_IP_PREFIX}.0.9
    depends_on:
      - thresh-test-mysql

  # nginx , 反向代理后端及 minio 等
  thresh-test-nginx:
    image: nginx
    container_name: thresh-test-nginx
    ports:
      - "12215:80"
    volumes:
      - ${THRESH_DEV_BASE_DIR}/nginx/conf/nginx.conf:/etc/nginx/nginx.conf
      - ${THRESH_DEV_BASE_DIR}/nginx/conf/conf.d:/etc/nginx/conf.d
      - ${THRESH_DEV_BASE_DIR}/nginx/log:/var/log/nginx
      - ${THRESH_DEV_BASE_DIR}/nginx/html:/usr/share/nginx/html
    networks:
      thresh-test:
        ipv4_address: ${THRESH_DEV_NETWORK_IP_PREFIX}.0.10
    extra_hosts:
      - "host.docker.internal:host-gateway"
    depends_on:
      - thresh-test-app-01
      - thresh-test-app-02

  # 平台端
  platform:
    image: khl/thresh-test-ui-admin:0.1
    container_name: thresh-test-ui-admin
    ports:
      - "12216:80"
    volumes:
      - ${THRESH_DEV_BASE_DIR}/thresh-test-ui-admin/log:/var/log/nginx
    networks:
      thresh-test:
        ipv4_address: ${THRESH_DEV_NETWORK_IP_PREFIX}.0.11
    extra_hosts:
      - "api.thresh-test.com:${THRESH_DEV_NETWORK_IP_PREFIX}.0.10"
      - "image.thresh-test.com:${THRESH_DEV_NETWORK_IP_PREFIX}.0.10"

  # pc 端
  portal:
    image: khl/thresh-test-ui-portal:0.1
    container_name: thresh-test-ui-portal
    ports:
      - "12217:80"
    volumes:
      - ${THRESH_DEV_BASE_DIR}/thresh-test-ui-portal/log:/var/log/nginx
    networks:
      thresh-test:
        ipv4_address: ${THRESH_DEV_NETWORK_IP_PREFIX}.0.12
    extra_hosts:
      - "api.thresh-test.com:${THRESH_DEV_NETWORK_IP_PREFIX}.0.10"

  # 商家端
  seller:
    image: khl/thresh-test-ui-seller:0.1
    container_name: thresh-test-ui-seller
    ports:
      - "12218:80"
    volumes:
      - ${THRESH_DEV_BASE_DIR}/threshtest-ui-seller/log:/var/log/nginx
    networks:
      thresh-test:
        ipv4_address: ${THRESH_DEV_NETWORK_IP_PREFIX}.0.13
    extra_hosts:
      - "api.thresh-test.com:${THRESH_DEV_NETWORK_IP_PREFIX}.0.10"
      
networks:
  thresh-test:
    driver: bridge
    ipam:
      driver: default
      config:
        - subnet: ${THRESH_DEV_NETWORK_IP_PREFIX}.0.0/16
          gateway: ${THRESH_DEV_NETWORK_IP_PREFIX}.0.1
