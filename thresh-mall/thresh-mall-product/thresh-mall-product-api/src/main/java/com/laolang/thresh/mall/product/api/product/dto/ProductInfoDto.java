package com.laolang.thresh.mall.product.api.product.dto;

import lombok.Data;

/**
 * 内部商品 api , 商品信息.
 *
 * @author laolang
 * @version 0.1
 */
@Data
public class ProductInfoDto {

    private Long id;
    private String title;
}
