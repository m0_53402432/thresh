package com.laolang.thresh.mall.product.api.product;

import com.laolang.thresh.mall.product.api.product.dto.ProductInfoDto;

/**
 * 内部商品 api.
 *
 * @author laolang
 * @version 0.1
 */
public interface ProductApi {

    ProductInfoDto info(Long id);
}
