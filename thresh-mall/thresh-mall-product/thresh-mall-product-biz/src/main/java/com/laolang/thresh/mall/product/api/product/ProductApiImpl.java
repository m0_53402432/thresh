package com.laolang.thresh.mall.product.api.product;

import com.laolang.thresh.mall.product.api.product.dto.ProductInfoDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 内部商品 api 实现.
 *
 * @author laolang
 * @version 0.1
 */
@Slf4j
@Service
public class ProductApiImpl implements ProductApi {

    @Override
    public ProductInfoDto info(Long id) {
        log.info("mall product api info");
        ProductInfoDto dto = new ProductInfoDto();
        dto.setId(1001L);
        dto.setTitle("安卓手机");
        return dto;
    }
}
