package com.laolang.thresh.mall.order.platform.rsp;

import lombok.Data;

/**
 * 平台订单详情 vo.
 *
 * @author laolang
 * @version 0.1
 */
@Data
public class OrderInfoRsp {

    private Long id;
    private Long productId;
    private String productTitle;
}
