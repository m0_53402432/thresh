package com.laolang.thresh.mall.order.platform.controller;

import com.laolang.thresh.framework.common.domain.R;
import com.laolang.thresh.mall.order.platform.logic.OrderPlatformLogic;
import com.laolang.thresh.mall.order.platform.rsp.OrderInfoRsp;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 平台订单管理.
 *
 * @author laolang
 * @version 0.1
 */
@RequiredArgsConstructor
@RequestMapping("mall/order/platform")
@RestController
public class OrderPlatformController {

    private final OrderPlatformLogic orderPlatformLogic;

    @GetMapping("info")
    public R<OrderInfoRsp> info() {
        return R.ok(orderPlatformLogic.info());
    }

}
