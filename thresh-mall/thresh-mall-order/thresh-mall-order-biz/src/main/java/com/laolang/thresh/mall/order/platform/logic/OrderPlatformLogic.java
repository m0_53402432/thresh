package com.laolang.thresh.mall.order.platform.logic;

import cn.hutool.json.JSONUtil;
import com.laolang.thresh.mall.order.platform.rsp.OrderInfoRsp;
import com.laolang.thresh.mall.product.api.product.ProductApi;
import com.laolang.thresh.mall.product.api.product.dto.ProductInfoDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 平台订单管理实现.
 *
 * @author laolang
 * @version 0.1
 */
@Slf4j
@RequiredArgsConstructor
@Service
public class OrderPlatformLogic {

    private final ProductApi productApi;

    public OrderInfoRsp info() {
        log.info("mall order platform info");
        OrderInfoRsp rsp = new OrderInfoRsp();
        rsp.setId(1L);
        ProductInfoDto productInfoDto = productApi.info(1001L);
        rsp.setProductId(productInfoDto.getId());
        rsp.setProductTitle(productInfoDto.getTitle());
        log.info("order info is:{}", JSONUtil.toJsonStr(rsp));
        return rsp;
    }
}
