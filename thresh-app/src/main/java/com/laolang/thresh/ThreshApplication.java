package com.laolang.thresh;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 项目启动类.
 *
 * @author laolang
 * @version 0.1
 */
@Slf4j
@SpringBootApplication
public class ThreshApplication {

    public static void main(String[] args) {
        SpringApplication.run(ThreshApplication.class, args);
        log.info("thresh is running...");
    }
}
