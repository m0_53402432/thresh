package com.laolang.thresh.module.system.user.controller;

import com.laolang.thresh.framework.common.domain.R;
import com.laolang.thresh.module.system.user.entity.SysDictType;
import com.laolang.thresh.module.system.user.logic.SysDictLogic;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 系统字典管理.
 *
 * @author laolang
 * @version 0.1
 */
@RequiredArgsConstructor
@RequestMapping("system/dict")
@RestController
public class SysDictController {

    private final SysDictLogic sysDictLogic;

    @GetMapping("detail")
    public R<SysDictType> dictDetail() {
        return R.ok(sysDictLogic.dictDetail());
    }


}
