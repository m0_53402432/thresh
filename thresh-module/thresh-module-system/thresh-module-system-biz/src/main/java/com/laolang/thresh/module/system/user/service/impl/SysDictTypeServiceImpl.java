package com.laolang.thresh.module.system.user.service.impl;

import com.laolang.thresh.framework.mybatis.core.BaseServiceImpl;
import com.laolang.thresh.module.system.user.entity.SysDictType;
import com.laolang.thresh.module.system.user.mapper.SysDictTypeMapper;
import com.laolang.thresh.module.system.user.service.SysDictTypeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * 系统字典 service 实现.
 *
 * @author laolang
 * @version 0.1
 */
@RequiredArgsConstructor
@Service
public class SysDictTypeServiceImpl extends BaseServiceImpl<SysDictTypeMapper, SysDictType> implements
    SysDictTypeService {

    private final SysDictTypeMapper sysDictTypeMapper;
}
