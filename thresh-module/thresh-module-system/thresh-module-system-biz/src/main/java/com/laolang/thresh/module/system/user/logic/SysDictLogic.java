package com.laolang.thresh.module.system.user.logic;

import com.laolang.thresh.module.system.user.entity.SysDictType;
import com.laolang.thresh.module.system.user.service.SysDictTypeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 系统字典管理逻辑实现.
 *
 * @author laolang
 * @version 0.1
 */
@RequiredArgsConstructor
@Slf4j
@Service
public class SysDictLogic {

    private final SysDictTypeService sysDictTypeService;

    public SysDictType dictDetail() {
        return sysDictTypeService.getById(1L);
    }
}
