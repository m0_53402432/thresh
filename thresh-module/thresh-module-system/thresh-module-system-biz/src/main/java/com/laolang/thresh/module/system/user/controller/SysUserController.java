package com.laolang.thresh.module.system.user.controller;

import com.laolang.thresh.framework.common.domain.R;
import com.laolang.thresh.module.system.user.logic.SysUserLogic;
import com.laolang.thresh.module.system.user.rsp.UserListRsp;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 系统用户管理.
 *
 * @author laolang
 * @version 0.1
 */
@RequiredArgsConstructor
@RequestMapping("system/user")
@RestController
public class SysUserController {

    private final SysUserLogic sysUserLogic;

    @GetMapping("list")
    public R<UserListRsp> list() {
        return R.ok(sysUserLogic.list());
    }
}
