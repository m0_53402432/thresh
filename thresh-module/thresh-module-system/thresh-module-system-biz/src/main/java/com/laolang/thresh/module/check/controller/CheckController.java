package com.laolang.thresh.module.check.controller;

import com.google.common.collect.Maps;
import com.laolang.thresh.framework.common.domain.R;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 供 jenkins 检查服务是否启动.
 *
 * @author laolang
 * @version 0.1
 */
@Slf4j
@RequestMapping("check")
@RestController
public class CheckController {

    @Value("${app.domain.tt}")
    private String tt;

    @GetMapping("pipeline")
    public R<Map<String, Object>> pipeline() {
        log.info("tt:{}", tt);
        Map<String, Object> map = Maps.newHashMap();
        map.put("tt", tt);
        return R.ok(map);
    }
}
