package com.laolang.thresh.module.system.user.logic;

import com.laolang.thresh.module.system.user.rsp.UserListRsp;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 系统用户业务实现.
 *
 * @author laolang
 * @version 0.1
 */
@Slf4j
@Service
public class SysUserLogic {

    public UserListRsp list() {
        log.info("system user list");
        UserListRsp rsp = new UserListRsp();
        rsp.setTotal(1L);
        return rsp;
    }
}
