package com.laolang.thresh.module.system.user.rsp;

import lombok.Data;

/**
 * 系统用户列表 vo.
 *
 * @author laolang
 * @version 0.1
 */
@Data
public class UserListRsp {

    private Long total;
}
